export class DestinoViaje{
    //atributos -> propiedades de la clase
    nombre: string;
    imagenUrl: string;

    //metodos -> acciones a realizar
    constructor(n: string, u: string){
        this.nombre = n;
        this.imagenUrl = u;
    }
} 